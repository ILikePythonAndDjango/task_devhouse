from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as ug
from django.utils.timezone import now
from datetime import time

DAY_OF_THE_WEEK = {
    '1' : ug(u'Monday'),
    '2' : ug(u'Tuesday'),
    '3' : ug(u'Wednesday'),
    '4' : ug(u'Thursday'),
    '5' : ug(u'Friday'),
    '6' : ug(u'Saturday'), 
    '7' : ug(u'Sunday'),
}

class DayOfTheWeekField(models.CharField):
    '''
    Class as model's Field for working with weeks
    '''
    def __init__(self, *args, **kwargs):
        kwargs['choices']=tuple(sorted(DAY_OF_THE_WEEK.items()))
        kwargs['max_length']=1 
        super(DayOfTheWeekField,self).__init__(*args, **kwargs)

class Shop(models.Model):

    title = models.CharField(max_length=100, unique=True)
    location = models.CharField(max_length=100)
    description = models.TextField()
    url_address = models.URLField(blank=True, null=True, unique=True)
    begun_from_with_us = models.DateField(auto_now_add=True)
    begun_from = models.DateField()
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

class Schedule(models.Model):

    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    begins = models.TimeField(blank=True, null=True)
    ends = models.TimeField(blank=True, null=True)
    is_opened_today = models.BooleanField(default=False)
    #`day of week` contains only number that representes as string
    day_of_week_as_id = DayOfTheWeekField()

    @property
    def day_the_week_as_string(self):
        return DAY_OF_THE_WEEK[self.day_of_week_as_id]

    @property
    def is_working_now(self):
        '''
        It's a boolean flag that indicates shop is working now or not.
        '''
        #converte datetime.datetime to datetime.time
        NOW = time(*now().timetuple()[3:6])
        if not self.is_opened_today:
            return False
        if self.begins < NOW and self.ends > NOW:
            #checking set of break that connection with this shop
            for br in Break.objects.filter(schedule=self.id).values('begins', 'ends'):
                if br['begins'] < NOW and br['ends'] > NOW:
                    return False
            return True
        return False

    def __str__(self):
        return "Schedule for {} on {}".format(self.shop.title, DAY_OF_THE_WEEK[self.day_of_week_as_id])

    class Meta:
        unique_together = (('shop', 'day_of_week_as_id'),)

class Break(models.Model):

    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    begins = models.TimeField()
    ends = models.TimeField()

    def __str__(self):
        return "Break that begins {} for {}".format(str(self.begins), str(self.schedule))

    class Meta:
        unique_together = (('begins', 'schedule'), ('ends', 'schedule'))