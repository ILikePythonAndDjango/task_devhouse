from django.contrib import admin
from .models import Shop, Schedule, Break

admin.site.register(Shop)
admin.site.register(Schedule)
admin.site.register(Break)
