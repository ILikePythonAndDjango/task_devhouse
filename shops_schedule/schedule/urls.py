from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'shops', views.ShopViewSet)
router.register(r'schedules', views.ScheduleViewSet)
router.register(r'breaks', views.BreakViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include('rest_auth.urls')),
    path('reg/', include('rest_auth.registration.urls')),
]
