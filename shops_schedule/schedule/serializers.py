from django.contrib.auth.models import User
from .models import Shop, Schedule, Break
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'url',
            'id',
            'username',
            'first_name',
            'last_name',
            'date_joined',
            'last_login',
            'is_active',
            'email',
        )

class ShopSerializer(serializers.ModelSerializer):

    #owner = UserSerializer()

    class Meta:
        model = Shop
        fields = (
            'url',
            'id',
            'title',
            'location',
            'description',
            'url_address',
            'begun_from_with_us',
            'begun_from',
            'owner',
        )

class ScheduleSerializer(serializers.ModelSerializer):

    #shop = ShopSerializer()

    class Meta:
        model = Schedule
        fields = (
            'url',
            'id',
            'shop',
            'begins',
            'ends',
            'is_opened_today',
            'day_of_week_as_id',
            'day_the_week_as_string',
            'is_working_now',
        )

#class ScheduleForBreakSerializer(serializers.ModelSerializer):

class BreakSerializer(serializers.ModelSerializer):

    #schedule = ScheduleSerializer()

    class Meta:
        model = Break
        fields = (
            'url',
            'id',
            'schedule',
            'begins',
            'ends',
        )