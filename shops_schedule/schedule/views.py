from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import get_object_or_404
from .models import Shop, Schedule, Break
from .serializers import *
from rest_framework import viewsets
from rest_framework.response import Response
from django.core.exceptions import ValidationError

def checking_user(view):

    def view_for_autheticated_user(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            return Response({"Error_message": "You're not authenticated!"})
        else:
            return view(self, request, *args, **kwargs)

    return view_for_autheticated_user

class UserViewSet(viewsets.ModelViewSet):

    http_method_names = ['get', 'head', 'options']

    serializer_class = UserSerializer
    queryset = User.objects.all()

class ShopViewSet(viewsets.ModelViewSet):

    http_method_names = ['get', 'put', 'post', 'head', 'options']

    serializer_class = ShopSerializer
    queryset = Shop.objects.all()

    @checking_user
    def create(self, request):
        try:
            Shop.objects.create(
                title = request.data.get('title'),
                location = request.data.get('location'),
                description = request.data.get('description'),
                url_address = request.data.get('url_address'),
                begun_from = request.data.get('begun_from'),
                owner = request.user
            )
        except ValidationError:
            return Response({"Error_message": "Data is incorrect!"})
        return Response({"Message": "Shop was created!"})

    @checking_user
    def update(self, request, pk=None):
        shop = get_object_or_404(Shop, pk=pk)
        if not shop.owner_id == request.user.id:
            return Response({"Error_message": "You're not owner!"})
        shop.title = request.data.get('title')
        shop.description = request.data.get('description')
        shop.url_address = request.data.get('url_address')
        shop.begun_from = request.data.get('begun_from')
        shop.save()
        return Response({"Message": "Shop's data was updated!"})


class ScheduleViewSet(viewsets.ModelViewSet):

    http_method_names = ['get', 'put', 'post', 'head', 'options']

    serializer_class = ScheduleSerializer
    queryset = Schedule.objects.all()

    @checking_user
    def update(self, request, pk=None):
        schedule = get_object_or_404(Schedule, pk=pk)
        shop = Shop.objects.get(id=schedule.shop_id)
        if not shop.owner_id == request.user.id:
            return Response({"Error_message": "You're not owner!"})
        schedule.shop = shop
        schedule.begins = request.data.get('begins')
        schedule.ends = request.data.get('ends')
        if schedule.ends < schedule.begins:
            return Response({"Error_message": "Incorrect time"})
        schedule.is_opened_today = True if request.data.get('is_opened_today') == 'true' else False
        schedule.day_of_week_as_id = request.data.get('day_of_week_as_id')
        schedule.save()
        return Response({"Message": "Schedule's data was updated!"})

class BreakViewSet(viewsets.ModelViewSet):

    http_method_names = ['get', 'put', 'post', 'head', 'options']

    serializer_class = BreakSerializer
    queryset = Break.objects.all()

    @checking_user
    def update(self, request, pk=None):
        br = get_object_or_404(Break, pk=pk)
        schedule = Schedule.objects.get(id=br.schedule_id)
        shop = Shop.objects.get(id=schedule.shop_id)
        if not shop.owner_id == request.user.id:
            return Response({"Error_message": "You're not owner!"})
        br.schedule = schedule
        br.begins = request.data.get('begins')
        br.ends = request.data.get('ends')
        if br.ends < br.begins:
            return Response({"Error_message": "Incorrect time"})
        br.save()
        return Response({"Message": "Break's data was updated!"})