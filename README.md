# task_devhouse

## About it

### Stack

Python==3.5.2

PostgreSQL==9.5.13

pytz==2018.4

Django==2.0.5

djangorestframework==3.8.2

django-rest-auth==0.9.3

django-allauth==0.36.0

### API

/users/ GET HEAD OPTIONS user-list

```
[
    {
        "url": "http://localhost:8000/users/4/",
        "id": 4,
        "username": "ColtOwner",
        "first_name": "Samuel",
        "last_name": "Colt",
        "date_joined": "2018-08-10T10:15:14Z",
        "last_login": null,
        "is_active": true,
        "email": ""
    },
    {
        "url": "http://localhost:8000/users/2/",
        "id": 2,
        "username": "seriy",
        "first_name": "Sergey",
        "last_name": "Sidorov",
        "date_joined": "2018-08-09T13:44:00Z",
        "last_login": "2018-08-10T11:13:38.686413Z",
        "is_active": true,
        "email": "sidorov@gmail.com"
    },
    {
        "url": "http://localhost:8000/users/5/",
        "id": 5,
        "username": "Sasha",
        "first_name": "",
        "last_name": "",
        "date_joined": "2018-08-10T11:32:59.657196Z",
        "last_login": null,
        "is_active": true,
        "email": "sasha_lol@gmail.com"
    }
]
```

/users/<int:pk>/ GET HEAD OPTIONS user-detail

```
{
    "url": "http://localhost:8000/users/13/",
    "id": 13,
    "username": "ewrrteewr",
    "first_name": "",
    "last_name": "",
    "date_joined": "2018-08-10T14:15:52.907965Z",
    "last_login": "2018-08-10T14:15:54.476011Z",
    "is_active": true,
    "email": "gmaijhkcl@gmail.com"
}
```

/shops/ GET, POST, HEAD, OPTIONS shop-list

Create shop with current user

```
[
    {
        "url": "http://localhost:8000/shops/3/",
        "id": 3,
        "title": "Colt's Manufacturing LLC",
        "location": "U.S.A., Texas",
        "description": "Colt's Manufacturing Company, LLC is an American firearms manufacturer, founded in 1855 by Samuel Colt. It is the successor corporation to Colt's earlier firearms-making efforts, which started in 1836",
        "url_address": "https://www.colt.com/",
        "begun_from_with_us": "2018-08-10",
        "begun_from": "1855-08-10",
        "owner": 4
    },
    {
        "url": "http://localhost:8000/shops/1/",
        "id": 1,
        "title": "Sex Shop",
        "location": "Russia, Rostov-on-Don",
        "description": "The sexiest sex shop in the world!!!dsfa",
        "url_address": "https://youtube.com",
        "begun_from_with_us": "2018-08-09",
        "begun_from": "2001-01-01",
        "owner": 2
    }
]
```

/shops/<int:pk>/ GET, PUT, PATCH, DELETE, HEAD, OPTIONS shop-detail

```
{
    "url": "http://localhost:8000/shops/3/",
    "id": 3,
    "title": "Colt's Manufacturing LLC",
    "location": "U.S.A., Texas",
    "description": "Colt's Manufacturing Company, LLC is an American firearms manufacturer, founded in 1855 by Samuel Colt. It is the successor corporation to Colt's earlier firearms-making efforts, which started in 1836",
    "url_address": "https://www.colt.com/",
    "begun_from_with_us": "2018-08-10",
    "begun_from": "1855-08-10",
    "owner": 4
}
```

/schedules/ GET, POST, HEAD, OPTIONS schedule-list

You can create only 7 schedules for one shop(`unique_together = (('shop', 'day_of_week_as_id'),)`)

```
[
    {
        "url": "http://localhost:8000/schedules/10/",
        "id": 10,
        "shop": 2,
        "begins": "06:00:00",
        "ends": "18:00:00",
        "is_opened_today": true,
        "day_of_week_as_id": "2",
        "day_the_week_as_string": "Tuesday",
        "is_working_now": true
    },
    {
        "url": "http://localhost:8000/schedules/11/",
        "id": 11,
        "shop": 2,
        "begins": "06:00:00",
        "ends": "18:00:00",
        "is_opened_today": true,
        "day_of_week_as_id": "3",
        "day_the_week_as_string": "Wednesday",
        "is_working_now": true
    },
    {
        "url": "http://localhost:8000/schedules/12/",
        "id": 12,
        "shop": 2,
        "begins": "06:00:00",
        "ends": "18:00:00",
        "is_opened_today": true,
        "day_of_week_as_id": "4",
        "day_the_week_as_string": "Thursday",
        "is_working_now": true
    },
    {
        "url": "http://localhost:8000/schedules/13/",
        "id": 13,
        "shop": 2,
        "begins": "06:00:00",
        "ends": "18:00:00",
        "is_opened_today": true,
        "day_of_week_as_id": "5",
        "day_the_week_as_string": "Friday",
        "is_working_now": true
    },
    {
        "url": "http://localhost:8000/schedules/14/",
        "id": 14,
        "shop": 2,
        "begins": "06:00:00",
        "ends": "18:00:00",
        "is_opened_today": true,
        "day_of_week_as_id": "6",
        "day_the_week_as_string": "Saturday",
        "is_working_now": false
    },
    {
        "url": "http://localhost:8000/schedules/15/",
        "id": 15,
        "shop": 2,
        "begins": null,
        "ends": null,
        "is_opened_today": false,
        "day_of_week_as_id": "7",
        "day_the_week_as_string": "Sunday",
        "is_working_now": false
    },
    {
        "url": "http://localhost:8000/schedules/9/",
        "id": 9,
        "shop": 2,
        "begins": "07:00:00",
        "ends": "18:00:00",
        "is_opened_today": true,
        "day_of_week_as_id": "1",
        "day_the_week_as_string": "Monday",
        "is_working_now": false
    }
]
```

/schedule/<int:pk>/ GET, PUT, DELETE, HEAD, OPTIONS schedule-detail

```
{
    "url": "http://localhost:8000/schedules/9/",
    "id": 9,
    "shop": 2,
    "begins": "07:00:00",
    "ends": "18:00:00",
    "is_opened_today": true,
    "day_of_week_as_id": "1",
    "day_the_week_as_string": "Monday",
    "is_working_now": false
}
```

/breaks/ GET, POST, HEAD, OPTIONS break-list

```
[
    {
        "url": "http://localhost:8000/breaks/2/",
        "id": 2,
        "schedule": 10,
        "begins": "15:00:00",
        "ends": "17:00:00"
    },
    {
        "url": "http://localhost:8000/breaks/3/",
        "id": 3,
        "schedule": 11,
        "begins": "06:00:00",
        "ends": "10:00:00"
    },
    {
        "url": "http://localhost:8000/breaks/4/",
        "id": 4,
        "schedule": 12,
        "begins": "20:00:00",
        "ends": "23:00:00"
    },
    {
        "url": "http://localhost:8000/breaks/5/",
        "id": 5,
        "schedule": 14,
        "begins": "10:00:00",
        "ends": "14:00:00"
    }
]
```

/breaks/<int:pk>/ GET, PUT, DELETE, HEAD, OPTIONS break detail

You can create break with only unique time (`unique_together = (('begins', 'schedule'), ('ends', 'schedule'))`)
```
{
    "url": "http://localhost:8000/breaks/1/",
    "id": 1,
    "schedule": 9,
    "begins": "11:00:00",
    "ends": "14:00:00"
}
```


